{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE Strict                     #-}
{-# LANGUAGE StrictData                 #-}

module Main where

import           Codec.Binary.UTF8.String
import           Control.Concurrent
import qualified Control.Exception.Base    as E
import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Maybe
import           Data.IORef
import           Data.List
import qualified Data.List.Split           as S
import qualified Data.Map.Strict           as M
import           Data.Maybe
import           Data.Monoid
import qualified Data.Text                 as T
import qualified Data.Text.IO              as TIO
import qualified Data.Text.Read            as TR
import           Data.Void
import           Debug.Trace
import           LinuxEcode
import           LinuxSignal
import           System.Console.GetOpt
import           System.Console.Haskeline  hiding (Handler, throwTo)
import           System.Directory          hiding (isSymbolicLink)
import           System.Environment
import           System.Exit
import qualified System.FilePath.Glob      as G
import           System.FilePath.Posix
import           System.IO
import           System.Posix.Files
import qualified System.Posix.Process      as P
import           System.Posix.Signals
import           System.Posix.Types
import           System.Posix.User
import           System.Process
import           System.Timeout
import           Text.Megaparsec
import           Text.Megaparsec.Char      hiding (spaces)
import           Text.Regex.Posix
import qualified TextShow                  as TS

data Env = Env { status :: Bool,
                 ret    :: Val,
                 args   :: [Val],
                 vars   :: M.Map T.Text (Bool, Val),
                 inn    :: Handle,
                 out    :: Handle,
                 err    :: Handle,
                 flags  :: Flags,
                 dir    :: String,
                 funcs  :: IORef (M.Map T.Text Val),
                 thread :: IORef ThreadInfo
                 }

sethandles denv senv = denv { out = out senv,
                              inn = inn senv,
                              err = err senv }

setRetEnv env renv = env { status = status renv,
                           ret    = ret renv }

defaultEnv :: FilePath -> IORef (M.Map T.Text Val) -> IORef ThreadInfo -> Env
defaultEnv =
  Env True
      (List [])
      []
      defaultVars
      stdin
      stdout
      stderr
      (Flags True False)

data Flags = Flags { funcSpaceAllocated     :: Bool,
                     ignoreInterpreterError :: Bool }

data ThreadInfo = ThreadInfo { tid      :: ThreadId,
                               exitMvar :: MVar Env,
                               cmdMvar  :: Maybe (MVar ()),
                               exitTrap :: Val
                               }

defaultVars :: M.Map T.Text (Bool, Val)
defaultVars = M.fromList [
  ("T", (True, Bool True)),
  ("F", (True, Bool False))
  ]

specialFuncs :: M.Map T.Text Val
specialFuncs = M.fromList [
  ("&&"    , Prim evalIf   ["COMMAND [ARG]..."]),
  ("||"    , Prim evalElse ["COMMAND [ARG]..."]),
  ("=>"    , Prim arrow    ["COMMAND [ARG]..."]),
  ("return", Prim returnB  ["[VALUE]..."]),
  ("succ"  , Prim succB    ["[VALUE]..."]),
  ("fail"  , Prim failB    ["[VALUE]..."]),
  ("exit"  , Prim exitB    ["[NUMBER]..."]),
  ("break" , Prim breakB   ["[{-s|-f}] [VALUE]..."]),
  ("let"   , Prim set      ["{-s|-r|-e|-o} NAME [{-s|-r|-e|-o} NAME]... COMMAND"
                           ,"VARIABLE VALUE"]),
  ("def"   , Prim def      ["{-s|-r|-e|-o} NAME [{-s|-r|-e|-o} NAME]... COMMAND"
                           ,"VARIABLE VALUE"]),
  ("fn"    , Prim fn       ["NAME COMMAND"]),
  ("trap"  , Prim trap     ["COMMAND SIGNAL..."]),
  ("cd"    , Prim cd       ["[DIR]"])
  ]

defaultFuncs :: M.Map T.Text Val
defaultFuncs = M.fromList [
  ("usage" , Prim usage    ["NAME"]),
  ("load"  , Prim load     ["FILE"]),
  ("loop"  , Prim loop     ["COMMAND [ARG]..."]),
  ("|"     , Prim pipe     ["COMMAND COMMAND..."]),
  ("not"   , Prim notB     ["COMMAND [ARG]..."]),
  ("read"  , Prim readB    ["[-i] [-n COUNT]"]),
  ("echo"  , Prim echo     ["[VALUE]..."]),
  ("show"  , Prim showB    ["VALUE..."]),
  ("get"   , Prim get      ["NAME"]),
  ("wait"  , Prim wait     ["VARIABLE..."]),
  ("poll"  , Prim poll     ["VARIABLE..."]),
  ("stop"  , Prim stop     ["[-f] VARIABLE..."]),
  ("glob"  , Prim glob     ["[-f] EXPRESSION..."]),
  ("splt"  , Prim split    ["[SEPARATOR] STRING"]),
  ("sub"   , Prim sub      ["REGEXP REPLACEMENT STRING"]),
  ("getenv", Prim getenv   ["NAME"]),
  ("setenv", Prim setenv   ["NAME STRING"]),
  ("map"   , Prim mapB     ["[-br] COMMAND [ARG]... LIST"]),
  ("fold"  , Prim foldB    ["[-br] COMMAND [ARG]... VALUE LIST"]),
  ("len"   , Prim lenB     ["[-c] VALUE"]),
  ("idx"   , Prim idx      ["NUMBER [NUMBER] LIST"]),
  ("timeo" , Prim timeo    ["DURATION COMMAND [ARG]..."]),
  ("exist?", Prim existB   ["[-bcdfLprsSwxOG]... FILE..."
                           ,"{-e|-n|-o} FILE..."]),
  ("list?" , Prim isList   ["value..."]),
  (":"     , Prim colon    ["VALUE..."]),
  ("spawn" , Prim spawn    ["COMMAND [ARG]..."]),
  ("try"   , Prim tryB     ["[-a] COMMAND [ARG]..."]),
  ("keep"  , Prim keepB    ["COMMAND [ARG]..."]),
  ("app"   , Prim app      ["VALUE..."])
  ]

isList env [] = return env{status=False, ret=List []}
isList env [x@List{}] = return env{status=True, ret=x}
isList env [x] = return env{status=False, ret=x}
isList env xs = return env{status=_isList xs, ret=List xs}
  where
    _isList []          = True
    _isList (List{}:xs) = _isList xs
    _isList _           = False

existB env xs = do
  (vs, opt) <- getOptEasy []
                 ["-b","-c","-d","-f","-L","-p","-r","-s","-S","-w","-x","-O","-G"
                 ,"-e","-n","-o"] "exist?" xs
  exclusiveOpt ["-e","-n","-o"] "exist?" opt
  xs <- mapM (expand env) vs
  case xs of
    [] -> Eval $ throwError (Int eINVAL, NumArgs "1 or more" 0)
    _ ->
      let paths = map (mkPath env) xs in do
        e <- and <$> liftIO (mapM fileExist paths)
        if e
          then do
            ys <- liftIO $ mapM (_existB paths) (M.keys opt)
                           `catchError` \e -> hPrint (err env) e >> return [False]
            return env{status=and ys, ret=List vs}
          else return env{status=False, ret=List vs}
  where
    _existB :: [String] -> T.Text -> IO Bool
    _existB files opt = do
      rs <- mapM getFileStatus files
      ss <- mapM getSymbolicLinkStatus files
      case opt of
        ""   -> return True
        "-b" -> return $ all isBlockDevice rs
        "-c" -> return $ all isCharacterDevice rs
        "-d" -> return $ all isDirectory rs
        "-f" -> return $ all isRegularFile rs
        "-L" -> return $ all isSymbolicLink ss
        "-p" -> return $ all isNamedPipe rs
        "-r" -> and <$> mapM (\x -> fileAccess x True False False) files
        "-s" -> return $ all ((/=0) . fileSize) ss
        "-S" -> return $ all isSocket rs
        "-w" -> and <$> mapM (\x -> fileAccess x False True False) files
        "-x" -> and <$> mapM (\x -> fileAccess x False False True) files
        "-O" -> (\y-> all (\x-> fileOwner x==y) rs) <$> getEffectiveUserID
        "-G" -> (\y-> all (\x-> fileGroup x==y) rs) <$> getEffectiveGroupID
        "-e" -> return $ ((== 1) $ length $ group $ map deviceID rs) &&
                         ((== 1) $ length $ group $ map fileID rs)
        "-n" -> return $ sortedBy (>=) $ map modificationTime rs
        "-o" -> return $ sortedBy (<=) $ map modificationTime rs
    sortedBy _ []       = True
    sortedBy _ [x]      = True
    sortedBy f (x:y:xs) = f x y && sortedBy f (y:xs)

usage env [x] = do
  n <- getVal x >>= expand env
  case usageShow n of
    Just s -> liftIO $ hPutStrLn (out env) s >> return env{status=True, ret=List []}
    _ -> Eval $ throwError (Int eINVAL, SomeError $ T.unpack n ++ " is not primitive function")

usageShow :: T.Text -> Maybe String
usageShow n =
  case M.lookup n $ M.union specialFuncs defaultFuncs of
    Just (Prim _ usage) -> Just $ T.unpack $
      case usage of
        [] -> "sorry, no usage"
        x:xs ->
          T.intercalate "\n" $ T.unwords ["Usage:", n, x]:map (\x -> T.unwords ["      ", n, x]) xs
    _ -> Nothing

fdToHandle :: Env -> Val -> Handle
fdToHandle env (FD 1) = out env
fdToHandle env (FD 2) = err env
fdToHandle env (FD 0) = inn env

writeCommon mode env (src:x:cmd) = do
  dst <- getVal x
  y <- liftIO $ case dst of
    String file ->
      case src of
        String "1" -> withFile (mkPath env file) mode
                               (\h -> runEval env{out=h} $ eval env{out=h} cmd)
        String "2" -> withFile (mkPath env file) mode
                               (\h -> runEval env{err=h} $ eval env{err=h} cmd)
    FD _ ->
      let h = fdToHandle env dst in
        case src of
          String "1" -> runEval env{out=h} $ eval env{out=h} cmd
          String "2" -> runEval env{err=h} $ eval env{err=h} cmd
  case y of
    Right renv -> return $ sethandles renv env
    Left e     -> Eval $ throwError e

writeOn = writeCommon WriteMode
addTo = writeCommon AppendMode

readFrom env (x:cmd) = do
  src <- getVal x
  case src of
    String file -> do
      e <- liftIO $ fileExist $ mkPath env file
      if e then do
        y <- liftIO $ withFile (mkPath env file) ReadMode
                      (\h -> runEval env $ eval env{inn=h} cmd)
        case y of
          Right renv -> return $ sethandles renv env
          Left e     -> Eval $ throwError e
      else
        Eval $ throwError (Int eNOENT, SomeError $ mkPath env file ++
                           ": no such file or directory")
    FD _ -> eval env{inn=fdToHandle env src} cmd

pipe env x = do
  mvs <- pipeEval env [] x
  synced@(tailEnv:_) <- liftIO $ mapM readMVar mvs
  case selectEcode synced of
    Nothing -> return $ env{status=True, ret=ret tailEnv}
    Just v  -> return $ env{status=False, ret=v}
  where
    pipeEval :: Env -> [MVar Env] -> [Val] -> Eval [MVar Env]
    pipeEval env [] (cmd:cmds) = do
      (i,o) <- liftIO createPipe
      mv <- spawnPipe env $
              do renv <- eval env{out=o} [cmd]
                 liftIO $ hClose o
                 return renv
      pipeEval env{inn=i} [mv] cmds
    pipeEval env mvs [cmd] = do
      mv <- spawnPipe env $
              do renv <- eval env [cmd]
                 liftIO $ hClose (inn env)
                 return renv
      return $ mv:mvs
    pipeEval env mvs (cmd:cmds) = do
      (i,o) <- liftIO createPipe
      mv <- spawnPipe env $
              do renv <- eval env{out=o} [cmd]
                 liftIO (hClose o >> hClose (inn env))
                 return renv
      pipeEval env{inn=i} (mv:mvs) cmds
    pipeEval env mvs [] = Eval $ throwError (Int eINVAL, NumArgs "1 or more" 0)

    selectEcode :: [Env] -> Maybe Val
    selectEcode [] = Nothing
    selectEcode (env:envs)
      | status env = selectEcode envs
      | otherwise  = Just $ ret env

    spawnPipe :: Env -> Eval Env -> Eval (MVar Env)
    spawnPipe env cmd = do
      mvar <- liftIO newEmptyMVar
      liftIO $ forkIO $
        do renv <- runEvalMain env cmd
           putMVar mvar renv
      return mvar

evalIf env x
  | status env = setRetEnv env <$> eval env x
  | otherwise  = return env

evalElse env x
  | not (status env) = setRetEnv env <$> eval env x
  | otherwise        = return env

arrow env x
  | status env = do
    renv <- eval env x
    throwError (ret renv, Returned $ status renv)
  | otherwise  = return env

cd env x = do
  path <- if null x
            then liftIO $ getEnv "HOME"
            else mkPath env <$> (getVal (head x) >>= expand env)
  e <- liftIO $ fileExist path
  if e
    then do
      f <- liftIO $ (isDirectory <$> getFileStatus path)
                    `catchError` \e -> hPrint (err env) e >> return False
      if f
        then return $ env {status=True, ret=Int 0, dir=normalise path}
        else Eval $ throwError (Int eNOENT, SomeError $ normalise path ++ " is not directory")
    else Eval $ throwError (Int eNOENT, SomeError $ normalise path ++ ": no such file or directory")

mkPath :: Env -> T.Text -> String
mkPath env x =
  let path = T.unpack x in
    normalise $ if head path == '/'
                  then path
                  else dir env </> path

load env [] = return env
load env x@(y:xs) = do
  file <- getVal y >>= expand env
  let path = mkPath env file in do
    e <- liftIO $ fileExist path
    if e then do
      code <- liftIO $ TIO.readFile (mkPath env file)
                       `catchError` \e -> hPrint (err env) e >> return ""
      case parse script "snale" code of
        Right val -> do
          x <- liftIO $ runEval env $ evalScript env val
          case x of
            Right renv -> load renv xs
            Left e     -> Eval $ throwError e
        Left e -> do
          liftIO $ hPrint (err env) e
          return env{status=False, ret=Int eINVAL}
    else
      Eval $ throwError (Int eNOENT, SomeError $ path ++ ": no such file or directory")

returnB env xs =
  case xs of
    []  -> Eval $ throwError (List [], Returned (status env))
    [x] -> Eval $ throwError (x, Returned (status env))
    _   -> Eval $ throwError (List xs, Returned (status env))

succB env xs =
  case xs of
    []  -> Eval $ throwError (List [], Returned True)
    [x] -> Eval $ throwError (x, Returned True)
    _   -> Eval $ throwError (List xs, Returned True)

failB env xs =
  case xs of
    []  -> Eval $ throwError (List [], Returned False)
    [x] -> Eval $ throwError (x, Returned False)
    _   -> Eval $ throwError (List xs, Returned False)

exitB env xs =
  case xs of
    [] -> Eval $ throwError (Int 0, Exited True)
    (x:_) -> do
      n <- getInt x
      case n of
        0 -> Eval $ throwError (Int 0, Exited True)
        _ | n > 0 -> Eval $ throwError (Int n, Exited False)
          | otherwise -> Eval $ throwError (Int eINVAL, SomeError $ show n ++ " is not natural number")

breakB env xs = do
  (arg, opt) <- getOptEasy [] ["-s","-f"] "break" xs
  exclusiveOpt ["-s","-f"] "break" opt
  case () of
    _| M.member "-s" opt -> _breakB env{status=True} arg
     | M.member "-f" opt -> _breakB env{status=False} arg
     | otherwise         -> _breakB env arg

_breakB env []  = Eval $ throwError (ret env, Broken (status env))
_breakB env [x] = Eval $ throwError (x, Broken (status env))
_breakB env xs  = Eval $ throwError (List xs, Broken (status env))

echo env xs = do
  x <- mapM (expand env) xs
  liftIO $ TIO.hPutStrLn (out env) $ T.unwords x
  return env{status=True, ret=Int 0}

showB env xs = do
  liftIO $ hPutStrLn (out env) $ unwords (map show xs)
  return env{ret=Int 0, status=True}

get env [] = return env
get env xs = do
  ys <- mapM (getVal >=> expand env) xs
  let zs = map (getVarMaybe env) ys in
    if Nothing `elem` zs
      then return env{status=False, ret=List []}
      else
        case map fromJust zs of
          [y] -> return env{status=True, ret=y}
          vs  -> return env{status=True, ret=List vs}

idx env xs =
  case xs of
    [x, List xs] -> do
      n <- getIntForIdx xs x
      return env{status=True, ret=xs !! (n-1)}
    [x, y, List xs] -> do
      n <- getIntForIdx xs x
      m <- getIntForIdx xs y
      return env{status=True, ret=List $ take (m-n+1) $ drop (n-1) xs}
    _ -> Eval $ throwError (Int eINVAL, NumArgs "2 or 3" $ length xs)

getIntForIdx :: [Val] -> Val -> Eval Int
getIntForIdx xs x = do
  n <- getInt x
  case n of
    0 -> Eval $ throwError (Int eINVAL, SomeError "0 cannot be specified")
    _ | abs n`less`xs -> Eval $ throwError (Int eINVAL, SomeError "Index too large")
      | n < 0 -> return $ length xs + n + 1
      | otherwise -> return n

wait env xs = do
  (ss, vs) <- unzip <$> mapM sync xs
  case vs of
    [v] -> return env{status=and ss, ret=v}
    _   -> return env{status=and ss, ret=List vs}
  where
    sync (Async (Threaded _ mv)) = do
      renv <- liftIO (readMVar mv)
      return (status renv, ret renv)
    sync (Async (Forked pid)) = waitpidVal pid
    sync x = return (True, x)

waitpidVal :: ProcessID -> Eval (Bool, Val)
waitpidVal pid = do
  x <- liftIO $ P.getProcessStatus True False pid
  case x of
    Just (P.Exited ExitSuccess) -> return (True, Int 0)
    Just (P.Exited (ExitFailure n)) -> return (False, Int n)
    Just (P.Terminated n _) -> return (False, Int $ fromIntegral $ toInteger n)
    Just _ -> Eval $ throwError (Int eINVAL, SomeError "Process is unexpected status")
    _ -> Eval $ throwError (Int eINVAL, SomeError "Failed to wait process")

poll env xs = do
  (ss, vs) <- unzip <$> mapM sync xs
  case vs of
    [v] -> return env{status=and ss, ret=v}
    _   -> return env{status=and ss, ret=List vs}
  where
    sync (Async (Threaded _ mv)) = do
      x <- liftIO (tryReadMVar mv)
      case x of
        Just renv -> return (True, List [Bool $ status renv, ret renv])
        _         -> return (False, List [])
    sync (Async (Forked pid)) = do
      x <- liftIO $ P.getProcessStatus False True pid
      case x of
        Just (P.Exited ExitSuccess) -> return (True, Int 0)
        Just (P.Exited (ExitFailure n)) -> return (True, Int n)
        Just (P.Terminated n _) -> return (True, Int $ fromIntegral $ toInteger n)
        Just (P.Stopped n) -> return (False, Int $ fromIntegral $ toInteger n)
        _ -> return (False, List [])
    sync x = return (True, List [Bool True, x])

stop env xs = do
  (xs, opt) <- getOptEasy [] ["-f"] "stop" xs
  liftIO$ mapM (_stop $ M.member "-f" opt) xs
  return env{status=True, ret=List []}
  where
    _stop f (Async (Threaded tid _)) =
      throwTo tid $ if f then E.ThreadKilled else E.UserInterrupt
    _stop f (Async (Forked pid)) =
      if f then signalProcessGroup killProcess pid
           else signalProcessGroup keyboardSignal pid
    _stop _ _ = return ()

set env xs = setterCommon env env "let" xs $ setterFn False

def env xs = setterCommon env env "def" xs $ setterFn True

setterFn :: Bool -> Env -> [Val] -> Val -> Eval Env
setterFn ro env [x] y = setVar env x (ro, y)
setterFn ro env xs y  = foldM (\e n->setVar e n (ro, y)) env xs

setVar :: Env -> Val -> (Bool, Val) -> Eval Env
setVar env x@Async{} v = getVal x >>= flip (setVar env) v
setVar env (String name) v
  | name == "_" = return env{status=True, ret=snd v}
  | otherwise =
    case M.lookup name $ vars env of
      Just (True, _)  -> Eval $ throwError (Int eINVAL, SomeError $
                                  show name ++ " is already defined")
      _ -> return $ (setVarVal env name v){status=True, ret=snd v}
setVar env (List x) (ro, List v) =
  case () of
    _| length x == length v -> do
         renv <- foldM (\e (n,y)->setVar e n (ro, y)) env $ zip x v
         if status renv then return renv
                        else return $ env{status=False, ret=ret renv}
     | otherwise -> return $ env{status=False, ret=List v}
setVar env _ (_, v) = return $ env{status=False, ret=v}

setVarVal :: Env -> T.Text -> (Bool, Val) -> Env
setVarVal env name v = env {vars=M.insert name v $ vars env}

setterCommon :: Env -> Env -> T.Text -> [Val] -> (Env -> [Val] -> Val -> Eval Env) -> Eval Env
setterCommon env set name xs@(_:_:_) fn = do
  (xs, opt) <- getOptEasy ["-r","-o","-e","-s"] [] name xs
  if opt == M.empty
    then fn set (init xs) (last xs)
    else do
      (env1, cc1) <- setterCommonOut opt env fn
      (env2, cc2) <- setterCommonErr opt env1 fn
      setterCommonRet opt env2 set fn xs >>= cc2 >>= cc1
  where
    setterCommonRet opt env set fn src =
      case src of
        [] -> Eval $ throwError (Int eNOENT, SomeError $ fromJust $ usageShow name)
        _ -> do
          renv <- eval env src
          rset <- case M.lookup "-r" opt of
                    Just xs -> fn set xs $ ret renv
                    _       -> return set
          case M.lookup "-s" opt of
            Just xs -> fn rset xs $ Bool $ status renv
            _       -> return rset
    setterCommonOut opt env fn =
      case M.lookup "-o" opt of
        Just xs -> do
          (i,o) <- liftIO createPipe
          return (env{out=o}, setOutput o i fn xs)
        Nothing -> return (env, return)
    setterCommonErr opt env fn =
      case M.lookup "-e" opt of
        Just xs -> do
          (i,o) <- liftIO createPipe
          return (env{err=o}, setOutput o i fn xs)
        Nothing -> return (env, return)
    setOutput o i fn xs set = do
      liftIO $ hClose o
      c <- liftIO $ TIO.hGetContents i
      fn set xs $ String $ delNewLn c
      where
        delNewLn c = case T.length c of
                       0 -> c
                       _ -> case T.last c of
                              '\n' -> delNewLn $ T.init c
                              _    -> c
setterCommon _ _ _ xs _ = Eval $ throwError (Int eINVAL, NumArgs "2 or more" $ length xs)

getOptEasy :: [T.Text] -> [T.Text] -> T.Text -> [Val] -> Eval ([Val], M.Map T.Text [Val])
getOptEasy havArg noArg name (String s:xs)
  | s == "--" = return (xs, M.empty)
  | T.isPrefixOf "--" s && elem s havArg = do
    (vs, ret) <- getOptEasy havArg noArg name $ tail xs
    case M.lookup s ret of
      Just _  -> return (vs, M.adjust (head xs:) s ret)
      Nothing -> return (vs, M.insert s [head xs] ret)
  | T.isPrefixOf "--" s && elem s noArg = do
    (vs, ret) <- getOptEasy havArg noArg name $ tail xs
    return (vs, M.insert s [] ret)
  | T.take 2 s `elem` havArg = do
    (arg, ys) <- case () of
                   _| T.length s == 2 && not (null xs) -> return (head xs, tail xs)
                    | T.length s == 2 ->
                      Eval $ throwError (Int eNOENT, SomeError $ fromJust $ usageShow name)
                    | otherwise -> return (String $ T.drop 2 s, xs)
    (vs, ret) <- getOptEasy havArg noArg name ys
    if M.member (T.take 2 s) ret
      then return (vs, M.adjust (arg:) (T.take 2 s) ret)
      else return (vs, M.insert (T.take 2 s) [arg] ret)
  | T.take 2 s `elem` noArg =
    let ys = if T.length s == 2
               then xs
               else (String $ "-" `T.append` T.drop 2 s):xs
    in do
      (vs, ret) <- getOptEasy havArg noArg name ys
      return (vs, M.insert (T.take 2 s) [] ret)
  | T.isPrefixOf "-" s && s /= "-" = Eval $ throwError (Int eNOENT, SomeError $ fromJust $ usageShow name)
getOptEasy havArg noArg name xs = return (xs, M.empty)

exclusiveOpt :: [T.Text] -> T.Text -> M.Map T.Text [Val] -> Eval ()
exclusiveOpt list name opt
  | length (M.filterWithKey (\x y -> elem x list) opt) < 2 = return ()
  | otherwise = Eval $ throwError (Int eNOENT, SomeError $ fromJust $ usageShow name)

fn env [mv@Async{}, body] = getVal mv >>= \x -> fn env [x, body]
fn env [String name, body] = do
  fs <- liftIO $ readIORef $ funcs env
  case M.lookup name fs of
    Nothing -> do
      liftIO $ writeIORef (funcs env) $ M.insert name body fs
      return env{status=True, ret=List []}
    _ -> Eval $ throwError (Int eINVAL, SomeError $ show name ++ " is already defined")
fn env [x, _] = Eval $ throwError (Int eINVAL, TypeMismatch "string" x)
fn env [] = return env
fn env x = Eval $ throwError (Int eINVAL, NumArgs "2" $ length x)

readB env xs = do
  (_ ,opt) <- getOptEasy ["-n"] ["-i"] "read" xs
  eof <- liftIO $ hIsEOF (inn env)
  if eof
    then
      return env{status=False, ret=List []}
    else
      case M.lookup "-n" opt of
        Just x -> do
          n <- getInt $ head x
          if n < 0 then
            Eval $ throwError (Int eNOENT, SomeError $ show n ++ " is not natural number")
          else do
            cs <- liftIO $ hGetNChar (M.member "-i" opt) n (inn env)
            if null cs then
              return env{status=False, ret=List []}
            else
              return env{status=True, ret=String $ T.pack cs}
        _ -> do
          x <- liftIO $ TIO.hGetLine (inn env)
          return env{status=True, ret=String x}
  where
    hGetNChar :: Bool -> Int -> Handle -> IO String
    hGetNChar _ 0 _ = return []
    hGetNChar f n h = do
      eof <- liftIO $ hIsEOF h
      if eof
        then return []
        else do
          c <- hGetChar h
          if f && c == '\n' then
            hGetNChar f n h
          else
            (:) c <$> hGetNChar f (n-1) h

glob env xs = do
  (xs, opt) <- getOptEasy [] ["-f"] "glob" xs
  case xs of
    [] -> Eval $ throwError (Int eINVAL, NumArgs "1 or more" $ length xs)
    _ -> do
      s <- mapM (getVal >=> expand env) xs
      p <- liftIO $ case M.lookup "-f" opt of
                      Just _ -> mapM ((\x -> G.globDir1 (G.compile x) (dir env)) . T.unpack) s
                      _ -> mapM ((\x -> _globDir1 x (dir env)) . T.unpack) s
      case p of
        [] -> return env {status=False, ret=List []}
        _  -> return env {status=True, ret=List $ map (String . T.pack) $ concat p}
  where
    _globDir1 x@('/':_) dir = G.globDir1 (G.compile x) dir
    _globDir1 x dir = map (makeRelative dir) <$> G.globDir1 (G.compile x) dir

split env x = do
  y <- mapM getVal x
  case y of
    [z, x] -> do
      s <- expand env z
      t <- expand env x
      return env{status=True, ret=List $ map String $ T.splitOn s t}
    [x] -> do
      t <- expand env x
      return env{status=True, ret=List $ map String $ T.words t}
    _   -> Eval $ throwError (Int eINVAL, NumArgs "1 or 2" $ length y)

spawn env xs = do
  (xs, opt) <- getOptEasy [] ["-b"] "spawn" xs
  if M.member "-b" opt then
    liftIO $ E.mask_ $ do
      hs <- signalHandleClear
      pid <- P.forkProcessWithUnmask $ \m -> do
        P.getProcessID >>= P.createProcessGroupFor
        tinfo <- allocThreadInfo
        runEvalMain env{thread=tinfo} (eval env{thread=tinfo} xs) >>= exitEval
      signalHandleRestore hs
      return env {status=True, ret=Async $ Forked pid}
  else do
    mvar <- liftIO newEmptyMVar
    jobID <- liftIO $
      forkFinally (do
        renv <- runEvalMain env $ eval env xs
        putMVar mvar renv)
        $ \_->putMVar mvar env{status=False, ret=Int 130}
    return env {status=True, ret=Async (Threaded jobID mvar)}

timeo env ys@(x:xs) = do
  n <- getInt x
  if n < 0 then
    Eval $ throwError (Int eNOENT, SomeError $ show n ++ " is not natural number")
  else do
    y <- liftIO $ timeout (n*1000000) $ runEval env $ eval env xs
    case y of
      Just (Right renv) -> return $ setRetEnv env renv
      Just (Left e)     -> Eval $ throwError e
      Nothing           -> return env{status=False, ret=List []}

tryB env xs = do
  (xs, opt) <- getOptEasy [] ["-a"] "try" xs
  renv <- if M.member "-a" opt
            then liftIO $ runEvalMain env $ eval env xs
            else do
              x <- liftIO $ runEvalTry env $ eval env xs
              case x of
                Right renv -> return renv
                Left e     -> Eval $ throwError e
  return $ setRetEnv env renv

keepB env xs = do
  x <- liftIO $ runEvalKeep env $ eval env xs
  case x of
    Right renv -> return renv
    Left e     -> Eval $ throwError e

notB env x = do
  renv <- eval env x
  if status renv
    then return renv{status=False}
    else return renv{status=True}

sub env x = do
  y <- mapM (getVal >=> expand env) x
  case map T.unpack y of
    [reg, rep, src] -> return env{status=True, ret=String $ T.pack $ subStr reg rep src}
      where
        subStr :: String -> String -> String -> String
        subStr reg rep src | match == "" = src
                           | otherwise = skipStr ++ (rep ++ subStr reg rep leftStr)
          where (skipStr, match, leftStr) = src =~ reg ::(String,String,String)
    _ -> Eval $ throwError (Int eINVAL, NumArgs "3" $ length y)

mapB env xs = do
  (zs, opt) <- getOptEasy [] ["-b","-r"] "map" xs
  case zs of
    _:_:_ -> do
      z <- mapM getVal zs
      case last z of
        List list -> do
          envs <- _mapM env (M.member "-b" opt) (M.member "-r" opt)
                        (\x-> eval env $ init z ++ [x]) list
          return env {ret=List $ map ret envs,
                      status=all status envs}
        _ -> do
          renv <- eval env z
          return env{ret=List [ret renv], status=status renv}
    _ -> Eval $ throwError (Int eINVAL, NumArgs "2 or more" $ length zs)
  where
    _mapM :: Env -> Bool -> Bool -> (Val -> Eval Env) -> [Val] -> Eval [Env]
    _mapM _ False False f xs = mapM f xs
    _mapM _ _ _ _ [] = return []
    _mapM env bopt ropt f (x:xs) = do
      y <- if ropt then
             case x of
               List ys -> do
                 envs <- _mapM env bopt ropt f ys
                 return env {ret=List $ map ret envs,
                             status=all status envs}
               _ -> f x
           else f x
      if not bopt || status y
        then (y:) <$> _mapM env bopt ropt f xs
        else return [y]

foldB env xs = do
  (zs, opt) <- getOptEasy [] ["-b","-r"] "fold" xs
  case zs of
    _:_:_:_ -> do
      z <- mapM getVal zs
      case last z of
        List ys ->
          setRetEnv env <$>
            _foldM (M.member "-b"opt) (M.member "-r" opt)
                   (\e x -> eval env $ take (length z - 2) z ++ ret e:[x])
                   env{ret=last $ init z} ys
        _ -> setRetEnv env <$> eval env z
    _ -> Eval $ throwError (Int eINVAL, NumArgs "3 or more" $ length zs)
  where
    _foldM :: Bool -> Bool -> (Env -> Val -> Eval Env) -> Env -> [Val] -> Eval Env
    _foldM False False f env xs = foldM f env xs
    _foldM _ _ _ env [] = return env
    _foldM bopt ropt f env (x:xs) = do
      y <- if ropt then
             case x of
               List ys ->
                 setRetEnv env <$>
                   _foldM bopt ropt f env ys
               _ -> f env x
           else f env x
      if not bopt || status y
        then _foldM bopt ropt f y xs
        else return y

lenB env xs = do
  (xs, opt) <- getOptEasy [] ["-c"] "len" xs
  if M.member "-c" opt then do
    l <- mapM getVal xs >>= foldM (\x y -> (x +) <$> clen y) 0
    return env{status=True, ret=Int l}
  else do
    ys <- mapM getVal xs
    return env{status=True, ret=Int $ foldl' (\x y -> x + olen y) 0 ys}
  where
    clen x = T.length . T.concat <$> expandL env x
    olen (List xs) = length xs
    olen _         = 1

getenv env [x] = do
  y <- getVal x >>= expand env >>= liftIO . getEnv . T.unpack
  return env{status=True, ret=String $ T.pack y}
getenv env x = Eval $ throwError (Int eINVAL, NumArgs "1" $ length x)

setenv env xs@[_, _] = do
  [n, v] <- map T.unpack <$> mapM (getVal >=> expand env) xs
  liftIO $ setEnv n v
  return env{status=True, ret=String $ T.pack v}
setenv env x = Eval $ throwError (Int eINVAL, NumArgs "2" $ length x)

loop env x@(cmd:arg) = do
  x <- liftIO $ runEvalFunc env $ eval env x
  case x of
    Right renv -> loop env [cmd, ret renv]
    Left e     -> Eval $ throwError e

trap env [] = Eval $ throwError (Int eINVAL, NumArgs "1 or more" 0)
trap env xs = do
  f:ys <- mapM getVal xs
  singnals <- case ys of
                [] -> return defaultSignal
                _  -> mapM txSignal ys
  liftIO $ do
    tinfo <- readIORef $ thread env
    case cmdMvar tinfo of
      Just _ -> return ()
      _ -> do
        mvar <- newEmptyMVar
        writeIORef (thread env) tinfo{cmdMvar=Just mvar}

  liftIO $ mapM (
             \x -> case x of
                     0 -> do tinfo <- readIORef (thread env)
                             writeIORef (thread env) tinfo{exitTrap=f}
                     _ -> do installHandler x (Catch $ trapHandler env f) Nothing
                             return ()
                 ) singnals
  return env{status=True, ret=List []}
  where
    trapHandler :: Env -> Val -> IO ()
    trapHandler env f = do
      x <- runEvalFunc env $ eval env [f]
      tinfo <- readIORef $ thread env
      let ThreadInfo tid exitMvar (Just cmdMvar) _ = tinfo in
        case x of
          Left (v, Exited s) -> do
            putMVar exitMvar env{status=s, ret=v}
            throwTo tid E.ThreadKilled
          _ -> putMVar cmdMvar ()
    txSignal (String s) =
      case M.lookup s signalMap of
        Just x -> return x
        _ -> Eval $ throwError (Int eINVAL, SomeError $ show s ++ " is not signal")

colon env []  = return env{status=True}
colon env [x] = return env{status=True, ret=x}
colon env xs  = return env{status=True, ret=List xs}

app env xs = do
  ys <- mapM getVal xs
  envs <- mapM (\x-> eval env [x]) ys
  return env {ret=List $ map ret envs, status=all status envs}

data Val = Int Int
         | Float Double
         | String T.Text
         | Bool Bool
         | FD Word
         | Var T.Text
         | LinkedStr [Val]
         | Lambda ArgFmt (Maybe Env) [[Val]]
         | List [Val]
         | Async JobInfo
         | Prim (Env -> [Val] -> Eval Env) [T.Text]
         | PrimFS (Env -> [Val] -> Eval Env)
         | Expr [Val]
         | LBr
         | RBr
         | ExprFn String RorL Word ArgNum ([(Bool, Val)] -> Eval (Bool, Val))
instance Show Val where
  show (Int x)            = show x
  show (Float x)          = show x
  show (String x)         = show x
  show (Bool True)        = "$T"
  show (Bool False)       = "$F"
  show (FD x)             = "&" ++ show x
  show (Var x)            = "$" ++ show x
  show (LinkedStr x)      = concatMap show x
  show (Lambda _ _ x)     = "{" ++ show x ++ "}"
  show (List x)           = show x
  show (Async x)          = show x
  show Prim{}             = "_PRIMITIVE_"
  show PrimFS{}           = "_PRIMITIVE_"
  show (Expr x)           = "(" ++ unwords (map show x) ++ ")"
  show LBr                = "("
  show RBr                = ")"
  show (ExprFn x _ _ _ _) = x
instance Eq Val where
  (==) (Int x) (Int y)       = x == y
  (==) (Float x) (Float y)   = x == y
  (==) (String x) (String y) = x == y
  (==) (Bool x) (Bool y)     = x == y
  (==) (FD x) (FD y)         = x == y
  (==) _ _                   = False
instance Ord Val where
  compare (Int x) (Int y)       = compare x y
  compare (Float x) (Float y)   = compare x y
  compare (String x) (String y) = compare x y
  compare (Int x) (Float y)     = compare (fromIntegral x) y
  compare (Int x) (String y)    = compare (T.pack $ show x) y
  compare (Float x) (Int y)     = compare x (fromIntegral y)
  compare (Float x) (String y)  = compare (T.pack $ show x) y

data JobInfo = Threaded ThreadId (MVar Env)
             | Forked ProcessID
instance Show JobInfo where
  show Threaded{} = "_THREAD_"
  show Forked{}   = "_PROCESS_"

getSyncVal :: Val -> Maybe Val
getSyncVal Async{} = Nothing
getSyncVal x       = Just x

getVal :: Val -> Eval Val
getVal (Async (Threaded _ mv)) = liftIO (readMVar mv) >>= getVal . ret
getVal (Async (Forked pid))    = snd <$> waitpidVal pid
getVal x                       = return x

valExpand :: Env -> [Val] -> Eval [Val]
valExpand _ [] = return []
valExpand env (Expr x:xs) = do
 y <- valExpand env x
 (Expr y :) <$> valExpand env xs
valExpand env (Var x:xs)
 | T.head x == '@' = do
   y <- (if x == "@" then getVar env "*"
                     else getVar env (T.tail x)) >>= getVal
   case y of
     List vs -> (vs ++) <$> valExpand env xs
     _       -> (y :)   <$> valExpand env xs
 | otherwise = do
   y <- getVar env x
   (y :) <$> valExpand env xs
valExpand env (Lambda f Nothing x:xs) = (Lambda f (Just env) x:) <$> valExpand env xs
valExpand env (List x:xs) = do
  v <- valExpand env x
  (:) (List v) <$> valExpand env xs
valExpand env (LinkedStr x:xs) = do
 y:ys <- valExpand env x >>= mapM (expandL env)
 (List (map String (foldl' expLinkedStr y ys)) :) <$> valExpand env xs
  where
    expLinkedStr xs ys = do
      x <- xs
      y <- ys
      return $ x `T.append` y
valExpand env (x:xs) = (x:) <$> valExpand env xs

valExpandMaybe :: Env -> Maybe Env -> [Val] -> Eval [Val]
valExpandMaybe _ (Just env) xs = valExpand env xs
valExpandMaybe env _ xs        = valExpand env xs

getVar :: Env -> T.Text -> Eval Val
getVar _ "~" = String . T.pack <$> liftIO (getEnv "HOME")
getVar x n = case getVarMaybe x n of
               Just val -> return val
               _        -> Eval $ throwError (Int ePERM, UnboundVar n)

getVarMaybe :: Env -> T.Text -> Maybe Val
getVarMaybe env "*" = Just $ List $ args env
getVarMaybe env "?" = Just $ ret env
getVarMaybe env x | Right (n, v) <- TR.decimal x =
  if v == "" || v == "*" then
    if length (args env) >= n then Just $ args env !! (n-1)
                              else Nothing
  else
    case getVarMaybe env v of
      Just (List vs) | length vs >= n -> Just $ vs !! (n-1)
      _                               -> Nothing
getVarMaybe env name =
  case M.lookup name $ vars env of
    Just (_, val) -> Just val
    _             -> Nothing

greater :: Int -> [a] -> Bool
greater n _      | n < 1 = True
greater _ []     = False
greater n (_:xs) = greater (n-1) xs

less x y = not $ greater x y

expand :: Env -> Val -> Eval T.Text
expand env (Int x) = return $ TS.showt x
expand env (Float x) = return $ TS.showt x
expand env (String x) = return x
expand env (Bool True) = return "True"
expand env (Bool False) = return "False"
expand env (FD x) = return $ "&" `T.append` TS.showt x
expand env (Lambda _ (Just fenv) vs) = return $ T.pack $ show vs
expand env (List vs) = T.unwords <$> mapM (expand env) vs
expand env x@Async{} = getVal x >>= expand env
expand env x = Eval $ throwError (Int eINVAL, TypeMismatch "expandable value" x)

expandL :: Env -> Val -> Eval [T.Text]
expandL env (Lambda _ (Just fenv) vs) = return $ map (T.pack . show) vs
expandL env (List vs)                 = concat <$> mapM (expandL env) vs
expandL env x@Async{}                 = getVal x >>= expandL env
expandL env x                         = (:[]) <$> expand env x

data RorL = R | L
  deriving Eq

data ArgNum = One | Two

yard :: [Val] -> [Val] -> Eval [Val]
yard y [] = return y
yard y (LBr:xs) = yard (LBr:y) xs
yard (LBr:ys) (RBr:xs) = yard ys xs
yard (y:ys) (RBr:xs) = (y:) <$> yard ys (RBr:xs)
yard (y@(ExprFn _ rl1 p1 _ _):ys) (x@(ExprFn _ rl2 p2 _ _):xs) =
  if (rl1 == L && p1 >= p2) || (p1 > p2)
    then (y:) <$> yard ys (x:xs)
    else yard (x:y:ys) xs
yard y (x@ExprFn{}:xs) = yard (x:y) xs
yard y (Expr x:xs) = yard y $ LBr:x ++ RBr:xs
yard y (x@Async{}:xs) = getVal x >>= \z -> yard y (z:xs)
yard y (Var x:xs) = Eval $ throwError (Int ePERM, Internal $ "Expanding Variable " ++ show x)
yard y (x:xs) = (x:) <$> yard y xs

evalExpr :: [(Bool, Val)] -> Val -> Eval [(Bool, Val)]
evalExpr (s:ss) (ExprFn _ _ _ One fn)     = (:ss) <$> fn [s]
evalExpr (s2:s1:ss) (ExprFn _ _ _ Two fn) = (:ss) <$> fn [s1, s2]
evalExpr ss x                             = return $ (True, x):ss

getInt :: Val -> Eval Int
getInt x =
  case readVal x of
    Just (Int n) -> return n
    Just (Float f) -> return $ floor f
    _ -> Eval $ throwError (Int eINVAL, SomeError $ show x ++ " cannot be read as number")

getNum :: Val -> Eval Val
getNum x =
  case readVal x of
    Just f -> return f
    _      -> Eval $ throwError (Int eINVAL, SomeError $ show x ++ " cannot be read as number")

readVal :: Val -> Maybe Val
readVal x =
  case x of
    String s ->
      case TR.signed TR.decimal s of
        Right (d, "") -> Just $ Int d
        _ -> case TR.double s of
               Right (d, "") -> Just $ Float d
               _             -> Nothing
    Int _ -> Just x
    Float _ -> Just x
    _ -> Nothing

readInt :: Val -> Maybe Int
readInt x =
  case x of
    String s ->
      case TR.signed TR.decimal s of
        Right (d, "") -> Just d
        _             -> Nothing
    Int d -> Just d
    Float f -> Just $ floor f
    _ -> Nothing

calc2Num :: (Int -> Int -> Int) -> (Double -> Double -> Double) ->
            (Bool, Val) -> (Bool, Val) -> Eval (Bool, Val)
calc2Num f1 f2 (_, x1) (_, x2) = do
  y1 <- getNum x1
  y2 <- getNum x2
  return $ (,) True $
    case y1 of
      Float z1 -> case y2 of
                    Float z2 -> Float $ f2 z1 z2
                    Int   z2 -> Float $ f2 z1 (fromIntegral z2)
      Int   z1 -> case y2 of
                    Float z2 -> Float $ f2 (fromIntegral z1) z2
                    Int   z2 -> Int $ f1 z1 z2

plus  (x1:x2:_) = calc2Num (+) (+) x1 x2
mul   (x1:x2:_) = calc2Num (*) (*) x1 x2
divB  (x1:x2:_) = calc2Num div (/) x1 x2
modB  (x1:x2:_) = calc2Num mod (\x y -> 0) x1 x2
pow   (x1:x2:_) = calc2Num (^) (**) x1 x2

minus [(_, x)] = do
  y <- getNum x
  case y of
    Int   n -> return (True, Int $ (-1) * n)
    Float n -> return (True, Float $ (-1) * n)
minus (x1:x2:_) = calc2Num (-) (-) x1 x2

calc2Bool :: (Val -> Val -> Bool) ->
             (Bool, Val) -> (Bool, Val) -> Eval (Bool, Val)
calc2Bool _ (False, x) _ = return (False, x)
calc2Bool f (_, x1) (_, x2) = do
  y1 <- maybe (return x1) return $ readVal x1
  y2 <- maybe (return x2) return $ readVal x2
  return $
    if f y1 y2
      then (True, y2)
      else (False, y1)

gt (x1:x2:_) = calc2Bool (>)  x1 x2
ge (x1:x2:_) = calc2Bool (>=) x1 x2
lt (x1:x2:_) = calc2Bool (<)  x1 x2
le (x1:x2:_) = calc2Bool (<=) x1 x2

notE ((b, x):_) = return (not b, x)

eq ((False, x):_:_) = return (False, x)
eq ((_, y@(List vs1)):(_, x@(List vs2)):_) = do
  ys <- zipWithM (\x y -> eq [(True, x), (True, y)]) vs1 vs2
  case find (not . fst) ys of
    Just _  -> return (False, y)
    Nothing -> return (True, x)
eq ((_, x1):(_, x2):_) = return $ if x1==x2 then (True, x2) else (False, x1)

same ((False, x):_:_) = return (False, x)
same ((_, y@(List vs1)):(_, x@(List vs2)):_) =
  if length vs1 == length vs2 then do
    ys <- zipWithM (\x y -> same [(True, x), (True, y)]) vs1 vs2
    case find (not . fst) ys of
      Just _  -> return (False, y)
      Nothing -> return (True, x)
  else
    return (False, y)
same ((_, x1):(_, x2):_) =
  case y1 of
    Just n1 -> case y2 of
                 Just n2 -> return $ if ans then (True, n2) else (False, n1)
                   where
                     ans =
                       case n1 of
                         Float z1 -> case n2 of
                                       Float z2 -> z1 == z2
                                       Int   z2 -> z1 == fromIntegral z2
                         Int   z1 -> case n2 of
                                       Float z2 -> fromIntegral z1 == z2
                                       Int   z2 -> z1 == z2

                 _       -> return $ if x1==x2 then (True, x2) else (False, x1)
    _ -> return $ if x1==x2 then (True, x2) else (False, x1)
  where
    y1 = readVal x1
    y2 = readVal x2

andB ((False, x):_:_)      = return (False, x)
andB (_:(True, y):_)       = return (True, y)
andB ((_, x):(False, _):_) = return (False, x)

orB ((False, x):(False, _):_) = return (False, x)
orB (_:(True, y):_)           = return (True, y)
orB ((_, x):_:_)              = return (True, x)

matchB ((False, x):_:_) = return (False, x)
matchB ((_, x):(_, y):_) = do
  list <- calcMatch x y
  case find ([] ==) list of
    Nothing -> return (True, List $ concat list)
    _       -> return (False, List [])

calcMatch :: Val -> Val -> Eval [[Val]]
calcMatch (String x) (String y) =
  case T.unpack x =~ T.unpack y of
    []    -> return [[]]
    [x]:_ -> return [[String $ T.pack x]]
calcMatch (Int n) (String m) =
  case show n =~ T.unpack m of
    []    -> return [[]]
    [x]:_ -> return [[String $ T.pack x]]
calcMatch (Float n) (String m) =
  case show n =~ T.unpack m of
    []    -> return [[]]
    [x]:_ -> return [[String $ T.pack x]]
calcMatch (List a) (List b) = do
  c <- zipWithM calcMatch a b
  return $ concat c
calcMatch _ _ = return [[]]

data ArgFmt = Positional
            | Ignoring

data ShError = Returned     Bool
             | Exited       Bool
             | Broken       Bool
             | SomeError    String
             | NumArgs      String Int
             | TypeMismatch String Val
             | UnboundVar   T.Text
             | Internal     String
instance Show ShError where
  show (SomeError    s)   = s
  show (Internal     s)   = "Internal error: " ++ s
  show (NumArgs      s n) = "Expected " ++ s ++ " args, found values " ++ show n
  show (TypeMismatch s v) = "Invalid type: expected " ++ s ++ ", found " ++ show v
  show (UnboundVar   t)   = "Getting an unbound variable: " ++ T.unpack t

newtype Eval a = Eval (ExceptT (Val, ShError) IO a)
  deriving (Functor,
            Applicative,
            Monad,
            MonadIO,
            MonadError (Val, ShError))

throwShError :: Env -> (Val, ShError) -> Eval a
throwShError env (v, e) = do liftIO $ hPrint (err env) (show e)
                             Eval $ throwError (v, e)

--
-- parser
--

type Parser = Parsec Void T.Text

spaceStr = " \t\r\v"
specialStr = "\n;|><&${}()[]'\"`-=#@"
spStrList = "\n${}()[]'\"`#@"
delimiterStr = spaceStr ++ specialStr

spaces = skipSome $ oneOf spaceStr
delSpaces = skipMany $ oneOf spaceStr
delBlankLine = skipMany $ oneOf $ '\n':spaceStr
comment = do char '#'
             skipMany $ noneOf ("\n" :: String)

delimiter :: Parser a -> Parser a
delimiter x = delSpaces >> x >>= (\y -> many (oneOf $ '\n':spaceStr) >> return y)

braced = between (char '{') (char '}')

quoted = between (char '\'') (char '\'')
dquoted = between (char '"') (char '"')
bquoted = between (char '`') (char '`')

brace :: ArgFmt -> Parser Val
brace f = do
  s <- braced script
  return $ Lambda f Nothing $
    case find isFn s of
      Just _ -> [Prim genFuncSpace []]:s
      _      -> s
  where
    isFn (PrimFS{}:_) = True
    isFn _            = False

list :: Parser Val
list = List <$> between (char '[') (char ']') listElem

varname :: Parser T.Text
varname = do
  x <- alphaNumChar <|> oneOf ("?*@" :: String)
  case x of
    '?' -> return $ T.singleton x
    '*' -> return $ T.singleton x
    '@' -> do
      ys <- optional varname
      case ys of
        Just xs -> return $ T.singleton x `T.append` xs
        Nothing -> return $ T.singleton x
    _   -> do
      xs <- many (alphaNumChar <|> oneOf ("_?:" :: String))
      return $ T.pack $ x:xs

variable :: Parser Val
variable = do
  char '$'
  x <- varname <|> braced varname
  return $ Var x

escaped :: Parser Char
escaped = do
  char '\\'
  x <- anyChar
  return $ case x of
    'n' -> '\n'
    'r' -> '\r'
    't' -> '\t'
    _   -> x

noEscaped :: Parser Char
noEscaped = noneOf delimiterStr <|>
              try (oneOf ("="::String) >>= \x -> notFollowedBy ">" >> return x) <|>
              try (char '-' >>= \x -> notFollowedBy (string ">" <|> string "->") >> return x) <|>
              try (char '@' >>= \x -> notFollowedBy (char '{' <|> char '(') >> return x)

noEscList :: Parser Char
noEscList = noneOf (spaceStr ++ spStrList) <|>
              try (char '@' >>= \x -> notFollowedBy (char '{' <|> char '(') >> return x)

escapedQuoted :: Char -> Parser Char
escapedQuoted c = try $ char c >> char c >> return c

quotedStr :: Parser Val
quotedStr = do
  x <- quoted  (many (escapedQuoted '\''<|> notChar '\'')) <|>
       dquoted (many (escapedQuoted '"' <|> notChar '"' )) <|>
       bquoted (many (escapedQuoted '`' <|> notChar '`' ))
  return $ String $ T.pack x

tilde :: Parser Val
tilde = try $ char '~' >> notFollowedBy (noneOf ("/\n" ++ delimiterStr)) >> return (Var "~")

str :: Parser Char -> Parser Val
str noEscFn = do
  x <- some (escaped <|> noEscFn)
  return $ String $ T.pack x

closure :: Parser Val
closure = do
  y <- optional $ char '@'
  let f = case y of
         Just _  -> Positional
         Nothing -> Ignoring
    in brace f <|> expr f

linkedStr :: Parser Char -> Parser Val
linkedStr noEscFn = do
  t <- optional tilde
  case t of
    Just v -> do
      x <- many (str noEscFn <|> quotedStr <|> variable <|> list <|> closure)
      case x of
        [] -> return v
        _  -> return $ LinkedStr $ v:x
    Nothing -> do
      x <- some (str noEscFn <|> quotedStr <|> variable <|> list <|> closure)
      case x of
        [y] -> return y
        _   -> return $ LinkedStr x

fd :: Parser Val
fd = FD . read <$> (char '&' >> some digitChar)

plusMinus :: (Word -> Parser [Val]) -> Word -> Parser [Val]
plusMinus fn 0 = return [RBr]
plusMinus fn n = do
  delSpaces
  op <- optional $ oneOf ("+-" :: String)
  ops <- fn n
  case op of
    Just '-' -> return $ ExprFn "-" R 6 One minus:ops
    _        -> return ops

exprVal :: Word -> Parser [Val]
exprVal 0 = return []
exprVal n = do
  delSpaces
  op <- optional $ operator <|> notOpStr <|> quotedStr <|> variable <|> list
  case op of
    Just LBr -> do
      ops <- plusMinus exprVal (n+1)
      return $ LBr:ops
    Just RBr ->
      case n-1 of
        0 -> return []
        m -> do
          ops <- exprVal m
          return $ RBr:ops
    Just x -> do
      ops <- exprVal n
      return $ x:ops
    _ -> return []

opChar :: String
opChar = "+-*/%&|=!~<>()^"

notOpStr :: Parser Val
notOpStr = do
  x <- some (noneOf $ opChar ++ delimiterStr)
  return $ String $ T.pack x

operator :: Parser Val
operator = do
  op <- oneOf opChar
  case op of
    '^' -> return $ ExprFn "^" R 7 Two pow
    '!' -> return $ ExprFn "!" R 6 One notE
    '*' -> return $ ExprFn "*" L 5 Two mul
    '/' -> return $ ExprFn "/" L 5 Two divB
    '%' -> return $ ExprFn "%" L 5 Two modB
    '+' -> return $ ExprFn "+" L 4 Two plus
    '-' -> return $ ExprFn "-" L 4 Two minus
    '&' -> do
      char '&'
      return $ ExprFn "&&" L 1 Two andB
    '|' -> do
      char '|'
      return $ ExprFn "||" L 1 Two orB
    '=' -> do
      op2 <- optional $ char '='
      case op2 of
        Nothing  -> return $ ExprFn "=" L 2 Two same
        Just '=' -> return $ ExprFn "==" L 2 Two eq
    '~' -> return $ ExprFn "~" L 2 Two matchB
    '<' -> do
      op2 <- optional $ char '='
      case op2 of
        Nothing  -> return $ ExprFn "<" L 3 Two lt
        Just '=' -> return $ ExprFn "<=" L 3 Two le
    '>' -> do
      op2 <- optional $ char '='
      case op2 of
        Nothing  -> return $ ExprFn ">" L 3 Two gt
        Just '=' -> return $ ExprFn ">=" L 3 Two ge
    '(' -> return LBr
    ')' -> return RBr

expr :: ArgFmt -> Parser Val
expr f = do
  char '('
  x <- plusMinus exprVal 1
  case f of
    Ignoring -> return $ Expr x
    _        -> return $ Lambda f Nothing [[Expr x]]

command :: Parser [Val]
command = delSpaces >> sepEndBy (linkedStr noEscaped) spaces

listElem :: Parser [Val]
listElem = skipMany (oneOf $ '\n':spaceStr)
           >> sepEndBy (linkedStr noEscList) (skipSome $ void (oneOf $ '\n':spaceStr) <|> comment)

redirection :: Parser [Val]
redirection = do
  delSpaces
  x <- string ">>" <|> string ">" <|> string "<"
  case x of
    ">" ->  (:) (Prim writeOn []) <$> ioSetting
    ">>" -> (:) (Prim addTo [])   <$> ioSetting
    "<" -> do
      delSpaces
      f <- linkedStr noEscaped <|> fd
      return [Prim readFrom [], f]
  where
    ioSetting :: Parser [Val]
    ioSetting = do
      y <- optional inputSetting
      delSpaces
      f <- linkedStr noEscaped <|> fd
      case y of
        Nothing ->
          return [String "1", f]
        Just [x] ->
          return [x, f]
        Just (x:xs) ->
          return $ x:f:Prim writeOn []:xs

inputSetting :: Parser [Val]
inputSetting =
  between (char '[') (char ']') $ do
    n <- some digitChar
    eq <- optional (char '=')
    case eq of
      Nothing -> return [String $ T.pack n]
      _ -> do
        m <- some digitChar
        return [String $ T.pack m, String $ T.pack n, FD $ read m]

_commandRd :: Parser ([Val], [Val])
_commandRd = do
   x <- command
   rd <- concat <$> many (try redirection)
   case rd of
     [] -> return (x, [])
     _ -> do
       (cmd2, rd2) <- _commandRd
       return (x ++ cmd2, rd ++ rd2)

commandRd :: Parser [Val]
commandRd = do
  (cmd, rd) <- _commandRd
  return $ rd ++ replaceFunc cmd
  where
    replaceFunc x@(String "fn":args) = PrimFS fn:args
    replaceFunc x@(String "load":args) = PrimFS load:args
    replaceFunc x@(String name:args) =
      case M.lookup name $ M.union specialFuncs defaultFuncs of
        Just fn -> fn:args
        _       -> x
    replaceFunc x = x

pipeP :: Parser [Val]
pipeP = try $ do
  x <- try $ sepBy1 commandRd (delimiter $ try (char '|' >> notFollowedBy (char '|')))
  case x of
    [x] -> return x
    _   -> return $ String "|":map List x

script :: Parser [[Val]]
script = do
  delBlankLine
  cmd <- bind "->" (Var "?")
     <|> bind "-->" (Var "@?")
     <|> shortCircuit "&&" (Prim evalIf [])
     <|> shortCircuit "||" (Prim evalElse [])
     <|> shortCircuit "=>" (Prim arrow [])
     <|> (delimiter (string ";") >> pipeP)
     <|> (delimiter (skipMany (comment >> delBlankLine)) >> pipeP)

  case cmd of
    []           -> return []
    String "|":x -> (:) (Prim pipe []:x) <$> script
    _            -> (:) cmd <$> script
  where
    shortCircuit :: T.Text -> Val -> Parser [Val]
    shortCircuit t f = do
      x <- delimiter $ string t
      (:) f <$> pipeP

    bind :: T.Text -> Val -> Parser [Val]
    bind t v = do
      x <- delimiter $ string t
      xs <- pipeP
      case xs of
        String "|":List y:ys -> return $ Prim pipe []:List (y ++ [v]):ys
        _                    -> return $ xs ++ [v]

--
-- eval
--

evalScript :: Env -> [[Val]] -> Eval Env
evalScript = foldM _eval
  where
    _eval env (PrimFS fn:xs) = valExpand env xs >>= fn env
    _eval env xs             = eval env xs

eval :: Env -> [Val] -> Eval Env
eval env [] = return env
eval env [List xs] = eval env xs
eval env x = do
  line <- valExpand env x
--  showB env line
  case line of
    [] -> return env
    cmd:args ->
      case cmd of
        Prim fn _ -> fn env args
        Expr vs        -> do
          y <- yard [] vs >>= foldM evalExpr []
          case y of
            []       -> return env{status=True, ret=List []}
            (s, v):_ -> return env{status=s, ret=v}
        x@Lambda{} -> evalFn env x args
        String x -> do
          funcs <- liftIO $ readIORef $ funcs env
          case M.lookup x funcs of
            Just fn ->
              if T.last x == '!'
                then evalMac env fn args
                else evalFn env fn args
            _ -> do
              expCmd <- expand env cmd
              expArgs <- mapM (expand env) args
              liftIO $ cmdExec env expCmd expArgs
                `catchError` \e -> hPrint (err env) e >> return env{status=False, ret=Int 127}
        PrimFS{} -> Eval $ throwError (Int eINVAL, SomeError "Can not define function in this context")
        _ -> Eval $ throwError (Int eINVAL, TypeMismatch "functon or string" cmd)

evalMac :: Env -> Val -> [Val] -> Eval Env
evalMac env (Lambda Positional _ body) arg = do
  renv <- evalScript env{args=arg} body
  return renv{args=args renv}
evalMac env (Lambda Ignoring _ body) args = evalScript env body
evalMac env x args = Eval $ throwError (Int eINVAL, TypeMismatch "functon" x)

evalFn :: Env -> Val -> [Val] -> Eval Env
evalFn env (Prim fn usage) args = fn env args
evalFn env (Lambda Positional fenv body) args = do
  genv <- genFuncEnv env fenv
  x <- liftIO $ runEvalFunc env $ evalScript genv{args=args} body
  case x of
    Right renv -> return $ setRetEnv env renv
    Left e     -> Eval $ throwError e
evalFn env (Lambda Ignoring fenv body) args = do
  genv <- genFuncEnv env fenv
  x <- liftIO $ runEvalFunc env $ evalScript genv body
  case x of
    Right renv -> return $ setRetEnv env renv
    Left e     -> Eval $ throwError e

genFuncEnv :: Env -> Maybe Env -> Eval Env
genFuncEnv env Nothing     =
--  rfs <- liftIO $ readIORef (funcs env) >>= newIORef
  return env{status=True}
genFuncEnv env (Just fenv) =
--  rfs <- liftIO $ readIORef (funcs fenv) >>= newIORef
  return env{status=True, ret=ret fenv, vars=vars fenv, funcs=funcs fenv}

genFuncSpace :: Env -> [Val] -> Eval Env
genFuncSpace env _ = do
  new <- liftIO $ readIORef (funcs env) >>= newIORef
  return env{funcs=new, flags=(flags env){funcSpaceAllocated=True}}

genFuncSpNoRead :: Env -> M.Map T.Text Val -> Eval Env
genFuncSpNoRead env src =
  if funcSpaceAllocated (flags env) then do
    liftIO $ writeIORef (funcs env) src
    return env
  else do
    new <- liftIO $ newIORef src
    return env{funcs=new, flags=(flags env){funcSpaceAllocated=True}}

cmdExec :: Env -> T.Text -> [T.Text] -> IO Env
cmdExec env cmd args = do
  (_,_,_,h) <- createProcess_ "snale"
                 (proc (T.unpack cmd) (map T.unpack args))
                 { std_in    = UseHandle $ inn env,
                   std_out   = UseHandle $ out env,
                   std_err   = UseHandle $ err env,
                   close_fds = True,
                   cwd       = Just $ dir env }
  ecode <- waitForProcess h
  case ecode of
    ExitSuccess     -> return env{status=True, ret=Int 0}
    ExitFailure ret ->
      if ret < 0
        then do
          tinfo <- readIORef $ thread env
          case cmdMvar tinfo of
            Just mvar -> takeMVar mvar >> return env{status=False, ret=Int ret}
            _ -> return env{status=False, ret=Int ret}
        else return env{status=False, ret=Int ret}

--
-- completion
--

wordlist :: Env -> IO [String]
wordlist env = do
  p <- getEnv "PATH"
  c <- mapM (\x -> listDirectory x `catchError` (\e->return [])) $ S.splitOn ":" p
  funcs <- readIORef $ funcs env
  return $ nub $ concat c ++ map T.unpack (M.keys funcs)

unquotedCompleteFn :: Env -> CompletionFunc IO
unquotedCompleteFn env line@(left,_)
  | isArg left   = completeFilename line
  | otherwise    = completeWord (Just '\\') " \t;|>&}" (searchfn env) line

searchfn :: Env -> String -> IO [Completion]
searchfn env s = do
  w <- wordlist env
  return $ map simpleCompletion $ filter (isPrefixOf s) w

isArg :: String -> Bool
isArg s =
  let x = words $ _isArg "" s in
    case x of
      []  -> False
      [_] -> case head s of
               ' '  -> True
               '\t' -> True
               _    -> False
      _ -> True
  where
    _isArg :: String -> String -> String
    _isArg x [] = x
    _isArg x xs =
       case xs of
         '|':'|':ys -> x
         '&':'&':ys -> x
         '>':'-':ys -> x
         ';':ys     -> x
         '|':ys     -> x
         '}':ys     -> x
         y  :ys     -> _isArg (y:x) ys

completeFn :: Env -> CompletionFunc IO
completeFn env = completeQuotedWord (Just '\\') "'" listFiles $ unquotedCompleteFn env

--
-- main ~ repl
--

repl :: FilePath -> Prefs -> Env -> String -> IO ()
repl home pref env str = do
  e <- fileExist $ home </> ".snalerc"
  renv <- if e then
           runEvalMain env (load env [String $ T.pack $ home </> ".snalerc"])
             `E.catch` killThreadHandler env
         else
           return env
  tryloop home renv >>= exitEval
  return ()
  where
    tryloop home env = do
      renv <- runInputTWithPrefs pref setting
                $ handle (\Interrupt -> return env)
                $ withInterrupt
                $ loop str ""
      setCurrentDirectory (dir renv)
      tryloop home renv
      where
        setting =
          Settings { historyFile = Just (home ++ "/.snale")
                   , complete = completeFn env
                   , autoAddHistory = True
                 }

        loop :: String -> T.Text -> InputT IO Env
        loop str lines = do
          x <- getInputLine str
          case x of
            Nothing -> liftIO $ exitEval env >> return env
            Just s  ->
              case parse script "snale" cmd of
                Right val ->
                  liftIO $ runEvalMain env $ evalScript env val
                Left err -> loop "  " cmd
              where
                cmd = mergeLines lines (T.pack s)
                mergeLines "" x = x`T.append`"\n"
                mergeLines y x  = y`T.append`x`T.append`"\n"


runEval :: Env -> Eval Env -> IO (Either (Val, ShError) Env)
runEval env (Eval fn) = runExceptT $ catchError fn $ handler env
  where
    handler :: Env -> (Val, ShError) -> ExceptT (Val, ShError) IO Env
    handler env e@(v, Returned s) = throwError e
    handler env e@(v, Broken s)   = throwError e
    handler env e@(v, Exited s)   = throwError e
    handler env e                 = ignoreError env e

runEvalFunc :: Env -> Eval Env -> IO (Either (Val, ShError) Env)
runEvalFunc env (Eval fn) = runExceptT $ catchError fn $ handler env
  where
    handler :: Env -> (Val, ShError) -> ExceptT (Val, ShError) IO Env
    handler env (v, Returned s) = return env{status=s, ret=v}
    handler env e@(v, Broken s) = throwError e
    handler env e@(v, Exited s) = throwError e
    handler env e               = ignoreError env e

runEvalKeep :: Env -> Eval Env -> IO (Either (Val, ShError) Env)
runEvalKeep env (Eval fn) = runExceptT $ catchError fn $ handler env
  where
    handler :: Env -> (Val, ShError) -> ExceptT (Val, ShError) IO Env
    handler env e@(v, Returned s) = throwError e
    handler env (v, Broken s)     = return env{status=s, ret=v}
    handler env e@(v, Exited s)   = throwError e
    handler env e                 = ignoreError env e

runEvalTry :: Env -> Eval Env -> IO (Either (Val, ShError) Env)
runEvalTry env (Eval fn) = runExceptT $ catchError fn $ handler env
  where
    handler :: Env -> (Val, ShError) -> ExceptT (Val, ShError) IO Env
    handler env e@(v, Returned s) = throwError e
    handler env e@(v, Broken s) = throwError e
    handler env e@(v, Exited s) = throwError e
    handler env (v, e) = liftIO (hPrint (err env) e) >> return env{status=False, ret=v}

ignoreError :: Env -> (Val, ShError) -> ExceptT (Val, ShError) IO Env
ignoreError env e@(v, x) = if ignoreInterpreterError $ flags env
                             then liftIO (hPrint (err env) x) >> return env{status=False, ret=v}
                             else throwError e

runEvalMain :: Env -> Eval Env -> IO Env
runEvalMain env (Eval fn) = do
  Right x <- runExceptT $ catchError fn $ handler env
  return x
  where
    handler :: Env -> (Val, ShError) -> ExceptT (Val, ShError) IO Env
    handler env (v, Returned s) = return env{status=s, ret=v}
    handler env (v, Broken s) = return env{status=s, ret=v}
    handler env (v, Exited s) = return env{status=s, ret=v}
    handler env (v, e) = liftIO (hPrint (err env) e) >> return env{status=False, ret=v}

killThreadHandler :: Env -> E.AsyncException -> IO Env
killThreadHandler env E.ThreadKilled = do
  tinfo <- readIORef $ thread env
  x <- tryReadMVar $ exitMvar tinfo
  case x of
    Just renv -> return renv
    _         -> return env{status=False, ret=Int 130}
killThreadHandler env e = E.throw e

allocThreadInfo :: IO (IORef ThreadInfo)
allocThreadInfo = do
  tid <- myThreadId
  exitMvar <- newEmptyMVar
  newIORef $ ThreadInfo tid exitMvar Nothing (Lambda Ignoring Nothing [])

signalHandleClear :: IO [Handler]
signalHandleClear =
  mapM (\x -> installHandler x Default Nothing)
    $ filter (\x -> not $ x == 0 || x == 9 || x == 19 || not (inSignalSet x fullSignalSet))
    $ M.elems signalMap

signalHandleRestore :: [Handler] -> IO [Handler]
signalHandleRestore h =
  mapM (\(x, y)-> installHandler y x Nothing)
    $ zip h
    $ filter (\x -> not $ x == 0 || x == 9 || x == 19 || not (inSignalSet x fullSignalSet))
    $ M.elems signalMap

options = [ Option ['c'] ["command"] (ReqArg id "COMMAND...") "command line" ]

trapExit :: Env -> IO Env
trapExit env = do
  tinfo <- readIORef $ thread env
  return env
  runEvalMain env $ eval env [exitTrap tinfo, ret env]

exitEval :: Env -> IO ()
exitEval env = do
  renv <- trapExit env
  if status renv
    then P.exitImmediately ExitSuccess
    else
      case readInt $ ret renv of
        Just n | n == 0 -> P.exitImmediately $ ExitFailure ePERM
               | n <= 0 -> P.exitImmediately $ ExitFailure 130
               | otherwise -> P.exitImmediately $ ExitFailure n
        _ -> P.exitImmediately $ ExitFailure ePERM

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stderr NoBuffering
  dir <- getCurrentDirectory
  funcs <- newIORef defaultFuncs
  env <- defaultEnv dir funcs <$> allocThreadInfo
  args <- getArgs
  case args of
    [] -> do
      home <- getEnv "HOME"
      pref <- readPrefs $ home </> ".snale_pref"
      repl home pref env  "@ "
    _ -> case getOpt RequireOrder options args of
           (cmds, [], []) -> do
             x <- mapM (\x -> case parse script "snale" $ T.pack x of
                                Right code -> runEvalMain env $ evalScript env code) cmds
             exitEval $ last x
           (_, file:args, []) -> do
             renv <- runEvalMain env (
                       load env{args=map (String . T.pack) args} [String $ T.pack file])
                       `E.catch` killThreadHandler env
             exitEval renv
           (_, _, es)   -> putStrLn (concat es) >> exitWith (ExitFailure eINVAL)
